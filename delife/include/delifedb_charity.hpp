#pragma once
#include <wasm.hpp>
#include <asset.hpp>
#include <table.hpp>

using namespace wasm;

namespace wasm { namespace db {

    #define TABLE_IN_CONTRACT [[wasm::table, wasm::contract("delife")]]

    struct TABLE_IN_CONTRACT project_t {
        uint64_t    id;                  //PK

        string      proj_code;   //15 chars
        uint64_t    total_donated   = 0;
        uint64_t    total_withdrawn = 0;
        uint64_t    created_at;

        uint64_t primary_key()const { return id; }
        uint64_t scope()const { return DELIFE_SCOPE; }

        project_t(uint64_t i): id(i) {}
        project_t() {}

        typedef wasm::table< "projects"_n, project_t, uint64_t > table_t;

        WASMLIB_SERIALIZE( project_t,   (id)(proj_code)
                                        (total_donated)(total_withdrawn)
                                        (created_at) )
    };


    // struct TABLE_IN_CONTRACT project_index_t {
    //     name                key_name;
    //     vector<uint64_t>    proj_ids;
    // };
    // typedef wasm::table< "projectidx"_n, project_index_t, string > project_index_tbl;

    struct TABLE_IN_CONTRACT donation_t {
        uint64_t            id;     //PK

        regid               donor;
        uint64_t            proj_id;
        uint64_t            amount;
        string              pay_sn;     //15
        uint64_t            donated_at;

        uint64_t primary_key() const { return id; }
        uint64_t       scope() const { return DELIFE_SCOPE; }

        donation_t() {}
        donation_t(uint64_t i): id(i) {}

        typedef wasm::table<"donations"_n, donation_t, uint64_t> table_t;

        WASMLIB_SERIALIZE( donation_t, (id)(donor)(proj_id)(amount)(pay_sn)(donated_at) )
    };


    struct TABLE_IN_CONTRACT acception_t {
        uint64_t            id;     //PK

        regid               donee;
        uint64_t            proj_id;
        uint64_t            recv_amount;
        string              pay_sn;
        uint64_t            received_at;


        uint64_t primary_key() const { return id; }
        uint64_t       scope() const { return DELIFE_SCOPE; }

        acception_t() {}
        acception_t(uint64_t i): id(i) {}

        typedef wasm::table<"acceptions"_n, acception_t, uint64_t> table_t;

        WASMLIB_SERIALIZE( acception_t, (id)(donee)(proj_id)(recv_amount)(pay_sn)(received_at) )
    };

} }