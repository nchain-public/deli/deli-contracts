project(delife)

set(WASM_WASM_OLD_BEHAVIOR "Off")
find_package(wasm.cdt)

add_contract( delife delife
                delife.cpp
                delife_utils.cpp
                delife_charity.cpp )
target_include_directories( delife PUBLIC ${CMAKE_SOURCE_DIR}/../include )
target_ricardian_directory( delife ${CMAKE_SOURCE_DIR}/../ricardian )
