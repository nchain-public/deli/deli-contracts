#include "delife.hpp"
#include "delifedb.hpp"
#include "delifedb_charity.hpp"

#include <chrono>
#include <system.hpp>
#include <rpc.hpp>

using std::chrono::system_clock;
using namespace wasm;
using namespace wasm::db;
using namespace wasm::safemath;

ACTION delife::add_project(regid issuer, string proj_code) {
    check_admin_auth(issuer);

    auto id = gen_new_id(COUNTER_PROJ);
    auto now = current_block_time();

    project_t project(id);
    check( !db::get(project), "project exists" );

    project.proj_code   = proj_code;
    project.created_at  = now;

    db::set(project);

    SHOWID( id )

}

ACTION delife::del_project(regid issuer, uint64_t proj_id) {
    check_admin_auth(issuer);

    project_t proj(proj_id);
    check( db::get(proj), "proj: " + to_string(proj_id) + " not exist" );

    db::del(proj);

}


/**
 * Usage:  to mint new CNY tokens upon receiving fiat CNY from another channel
 */
ACTION delife::donate(regid issuer, regid donor, uint64_t proj_id, asset quantity, string pay_sn, string memo) {
    check_admin_auth(issuer);
    check( is_account(donor), "donor account not exist" );

    auto id = gen_new_id(COUNTER_DONATION);
    auto now = current_block_time();
    auto bank = regid( get_config(CK_TOKEN_BANK_REGID) );

    project_t proj(proj_id);
    check( db::get(proj), "proj: " + to_string(proj_id) + " not exist" );
    proj.total_donated += quantity.amount;
    db::set(proj);

    donation_t donation(id);
    db::get(donation);

    donation.donor      = donor;
    donation.proj_id    = proj_id;
    donation.amount     = quantity.amount;
    donation.pay_sn     = pay_sn;
    donation.donated_at = now;

    db::set(donation);

    MINT( bank, _self, quantity )

    SHOWID( id )

}

/**
 * Usage:  transfer WICC/WGRT tokens to the contract for charity/shopping etc. purposes
 */
ACTION delife::accept(regid issuer, regid to, uint64_t proj_id, asset quantity, string pay_sn, string memo) {
    check_admin_auth(issuer);

    check( is_account(to), "to is not an account" );

    auto bank   = regid( get_config(CK_TOKEN_BANK_REGID) );
    auto now    = current_block_time();
    auto id     = gen_new_id(COUNTER_ACCEPTION);
    acception_t acception(id);
    db::get(acception);

    project_t proj(proj_id);
    check( db::get(proj), "proj: " + to_string(proj_id) + " not exist" );
    check( proj.total_donated >= proj.total_withdrawn, "proj over-withdrawn" );
    auto available_amount = proj.total_donated - proj.total_withdrawn;
    check( quantity.amount <= available_amount, "insufficient to donate" );
    proj.total_withdrawn   += quantity.amount;
    db::set(proj);

    acception.donee         = to;
    acception.proj_id       = proj_id;
    acception.recv_amount   = quantity.amount;
    acception.pay_sn        = pay_sn;
    acception.received_at   = now;

    BURN( bank, _self, quantity )

    SHOWID( id )
    
}